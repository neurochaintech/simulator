cmake_minimum_required(VERSION 3.0)
set (CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/CMake")
include(HunterGate)
HunterGate(
    URL "https://github.com/ruslo/hunter/archive/v0.19.87.tar.gz"
    SHA1 "1b0041a06357d2c9f1f1a7e93ff3132c36411719"
)
project(neurosim)

enable_testing ()


set(CPACK_PACKAGE_VERSION_MAJOR "0")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PACKAGE_VERSION_PATCH "1")
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")

set(CMAKE_CXX_FLAGS "-Wall  ${CMAKE_CXX_FLAGS} -std=c++17 ")
set(CMAKE_CXX_FLAGS_RELEASE " -O3 ${CMAKE_CXX_FLAGS}")
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0  ${CMAKE_CXX_FLAGS} --coverage -fsanitize=address  ")

include(GNUInstallDirs)
include(FindPkgConfig)
include(trax_test)

hunter_add_package(dlib)
find_package(dlib CONFIG REQUIRED)

hunter_add_package(Boost COMPONENTS program_options system filesystem)
find_package(Boost CONFIG REQUIRED program_options system filesystem)

hunter_add_package(GTest)
find_package(GTest CONFIG REQUIRED)


add_subdirectory(src)

set(CPACK_GENERATOR "TGZ")
set(CPACK_TAR_COMPONENT_INSTALL ON)
set(CPACK_COMPONENTS_IGNORE_GROUPS 1)
set(CPACK_COMPONENTS_ALL dev main)

include(CPack)
