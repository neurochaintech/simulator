#ifndef NEURO_SRC_NET_HPP
#define NEURO_SRC_NET_HPP

#include <vector>
#include <list>
#include <ctime>
#include <random>

#include "def.hpp"
#include "Message.hpp"

namespace neuro {

class Net {
public:
  using Messages = std::list<Message>;
  using Time = std::vector<Messages>;
  
private:
  Time _time;
  
  std::uniform_int_distribution<> _dis;
  std::time_t _ts; 

public:
  Net(const std::size_t max_time);

  void schedule(const Message &message,
		latency l,
		const bool alea = false);
  Messages* current_messages();  
  void incr ();

  decltype(_ts) ts() const {
    return _ts; 
  }

  bool has_data() const;
  
  friend std::ostream &operator<< (std::ostream &os, const Net &);
};

std::ostream &operator<< (std::ostream &os, const Net &);

}  // neuro

#endif /* NEURO_SRC_NET_HPP */
