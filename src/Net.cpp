#include <iomanip>

#include "Net.hpp"
#include "Message.hpp"
namespace neuro {

Net::Net(const std::size_t max_time) :
  _time(max_time),
  _dis(0, 100),
  _ts(0) {}


void Net::schedule(const Message &message,
		   latency l,
		   const bool alea) {
  if(alea) {
    l *= 1 + _dis(gen)/10.0;
  }

  if(l > _time.size()) {
    throw std::runtime_error("latency too hight");
  }

  _time[(l+_ts)%_time.size()].emplace_back(message);
}

Net::Messages* Net::current_messages() {
  return &_time[_ts%_time.size()];
}
  
void Net::incr () {
  _ts++;
}


std::ostream &operator<< (std::ostream &os, const Net &net) {
  os << "Time:" << std::setw(10) << net._ts << " -- ";

  for (std::size_t i = 0 ; i < net._time.size() ; i++) {
    os << net._time[(net._ts+i)%net._time.size()].size() << " ";
  }
  
  return os;
}

bool Net::has_data() const {
  for (const auto &messages : _time) {
    if(!messages.empty()) {
      return true;
    }
  }

  return false;
}

}  // neuro
