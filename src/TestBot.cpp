#include "Bot.hpp"
#include <gtest/gtest.h>

namespace neuro {

class TestBot : public ::testing::Test {
protected:
  
};

TEST(TestBot, Accessor) {
  Bot a(42);
  Bot b(17);
  EXPECT_EQ(a.id(), 42);

}

}  // neuro
