#include "sim.hpp"

#include "Message.hpp"
namespace neuro {

Simulator::Simulator(const uint32_t bots_count,
		     const uint8_t bot_connection_count,
		     const latency max_time):
  _bots(bots_count),
  _net(max_time),
  _msgs_received(0),
  _msgs_sent(0){
  Bot::_net = &_net;

  connect_random(bot_connection_count);
}

void Simulator::connect_random(const uint8_t bot_connection_count) {
  std::uniform_int_distribution<BotID> dis(0, _bots.size() -1);

  for (auto &bot : _bots) {
    for(uint8_t i = 0 ; i < bot_connection_count ; ++i) {
      if (const auto id = dis(gen); id != bot.id()) {
	bot.add_neightboor(id);
      }
    }
  }

}


void Simulator::run () {
  while (process()) {};
}

bool Simulator::process() {
  auto messages = _net.current_messages();

  while (!messages->empty()) {
    auto &message = messages->front();
    const auto id = message.addrs.back();
    auto *bot_current = _bots.bot_mutable(id);
    //message.addrs.push_back(bot_current->id());
    bot_current->dispatch(message);
    messages->pop_front();
  }
  _net.incr();
  return(_net.has_data());
}

void Simulator::send(const BotID src, const BotID dst) {
  Message message(17, dst);
  message.addrs.push_back(src);
  _bots.bot_mutable(src)->dispatch(message);

}

}  // neuro
